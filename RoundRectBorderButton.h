//
// Created by pbukhon on 15/09/2017.
// Copyright (c) 2017 Pavel Bukhonov. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface RoundRectBorderButton : UIButton

@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic, assign) IBInspectable BOOL round;
@property (nonatomic, assign) IBInspectable NSInteger linesCount;

@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor * borderColor;

@end