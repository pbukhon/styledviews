//
// Created by pbukhon on 15/09/2017.
// Copyright (c) 2017 Pavel Bukhonov. All rights reserved.
//

#import "RoundRectView.h"


@implementation RoundRectView

- (void)makeDefaultValues {
    _cornerRadius = 0;
    _round = NO;
    _borderColor = nil;
    _borderWidth = 0;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self makeDefaultValues];
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self makeDefaultValues];
        [self customInit];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [self customInit];
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
    [self setNeedsDisplay];
}

- (void)prepareForInterfaceBuilder {
    [self customInit];
}

-(void)customInit{
    if (_round)
        _cornerRadius = self.frame.size.height / 2;
    self.layer.cornerRadius = _cornerRadius;
    if (self.layer.cornerRadius > 0) {
        self.layer.masksToBounds = YES;
    }
    self.layer.borderWidth = _borderWidth;
    if (_borderColor) {
        self.layer.borderColor = _borderColor.CGColor;
    } else {
        self.layer.borderColor = self.backgroundColor.CGColor;
    }
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    self.layer.borderWidth = _borderWidth;
}

@end