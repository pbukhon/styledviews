//
// Created by pbukhon on 06/12/2017.
// Copyright (c) 2017 Pavel Bukhonov. All rights reserved.
//

#import "VSUnderlinedButton.h"
#import "UIColor+ColorWithHex.h"


@implementation VSUnderlinedButton {
    UIView * _underlineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _underlineView = [[UIView alloc] init];
    _underlineView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_underlineView];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[underlineView]|" options:nil metrics:nil views:@{@"underlineView" : _underlineView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[underlineView(1)]-6-|" options:nil metrics:nil views:@{@"underlineView" : _underlineView}]];
    self.selected = self.selected;
}


- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected){
        _underlineView.backgroundColor = [UIColor colorWithHex:0xfa4351];
    } else {
        _underlineView.backgroundColor = [UIColor colorWithHex:0x3a4143];
    }
}

@end